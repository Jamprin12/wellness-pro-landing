import Header from 'components/Header'
import Seo from 'components/Seo'

const Home: React.FC = () => {
  return (
    <>
      <Seo />
      <main>
        <Header />
      </main>
    </>
  )
}

export default Home
