export default function Hero() {
  return (
    <div className="hero">
      <h2 className="hero__title">
        Tenemos lo necesario para tu mejor versión
      </h2>
      <h6 className="hero__slogan">
        Comienza tu trayectoria a sacar la mejor versión de ti
      </h6>
      <div className="hero__buttongroup">
        <button className="hero__button">Ingresar</button>
        <button className="hero__button">Registrarse</button>
      </div>
    </div>
  )
}
