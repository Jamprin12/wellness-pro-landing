import Head from 'next/head'

interface SeoProps {
  title?: string
  description?: string
  image?: string
}

const Seo: React.FC<SeoProps> = ({ title: t, description, image }) => {
  const title = t ? `${t} | WellNess Pro` : 'WellNess Pro'

  return (
    <Head>
      <title>{title}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="description" content={description} />
      <meta name="title" content={title} />
      <meta httpEquiv="X-UA-Compatible" content="ie=edge" />

      <meta property="og:type" content="website" />
      <meta property="og:url" content="https://metatags.io/" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />

      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content="https://metatags.io/" />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={description} />
      <meta property="twitter:image" content={image} />
      <link rel="icon" href="/favicon.ico" />
      {/* <link rel="manifest" href="/manifest.json" /> */}
    </Head>
  )
}

export default Seo
